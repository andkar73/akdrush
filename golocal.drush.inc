<?php
/**
 * @file
 * Custom Drush integration.
 */
 
/**
 * Implements hook_drush_command().
 *
 * @return
 *   An associative array describing your command(s).
 */
function golocal_drush_command() {
  return array(
    'golocal' => array(
      'description' => dt('Puts so-rummet in local development mode.'),
    ),
  );
}
 
/**
 * Put the site in local development mode.
 */
function drush_golocal() {
  // Enable dev friendly modules.
  module_enable(array('devel'), TRUE);
 
  // Disable any production modules that you I don't want to run locally.
  $disable = array(
      'memcache', 
      'memcache_admin', 
      'xmlsitemap', 
      'xmlsitemap_menu',
      'xmlsitemap_node',
      'xmlsitemap_taxonomy',
      'googleanalytics',
      'clear_varnish_page_cache',
      'varnish',
      );
  module_disable($disable);
  drush_log(dt('Modules disabled: @modules', array('@modules' => implode(', ', $disable))), 'ok');
 
  // Allow everyone to see devel messages like dpm().
  if(module_exists('devel')) {
    user_role_grant_permissions(1, array('access devel information'));
    user_role_grant_permissions(2, array('access devel information'));
  }
 
  // Set some dev-friendly settings
  variable_set('cache', "0");
  variable_set('block_cache', "0");
  variable_set('error_level', "2");
  variable_set('preprocess_js', "0");
  variable_set('preprocess_css', "0");
  variable_set('page_compression', "0");
  drush_log('Page cache, page compression, JS optimization, and CSS optimization disabled.', 'ok');
 
  drupal_flush_all_caches();
  drush_log('All caches cleared.', 'ok');
 
  drush_log('Site ready for development!', 'ok');
}
 

